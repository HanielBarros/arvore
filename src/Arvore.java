import java.util.Stack;

public class Arvore {

	private No mae;

	public void add(int valor) {
		add(mae, valor);
	}

	public void add(No n, int valor) {
		if (n == null) {
			mae = new No(valor);
		} else if (valor < n.getValor()) {
			if (n.getEsquerda() != null) {
				add(n.getEsquerda(), valor);
			} else {
				n.setEsquerda(new No(valor));
			}
		} else {
			if (n.getDireita() != null) {
				add(n.getDireita(), valor);
			} else {
				n.setDireita(new No(valor));
			}
		}
	}

	public int getAltura() {
		return getAltura(mae);
	}

	private int getAltura(No n) {
		if (n == null) {
			return 0;
		}

		int direita = getAltura(n.getDireita());
		int esquerda = getAltura(n.getEsquerda());

		if (direita >= esquerda) {
			return direita + 1;
		} else {
			return esquerda + 1;
		}
	}

	public int pegaQtdNo() {
		return pegaQtdNo(mae);
	}

	private int pegaQtdNo(No no) {
		if (no == null) {
			return 0;
		}

		int qtdEsquerda = pegaQtdNo(no.getEsquerda());
		int qtdDireita = pegaQtdNo(no.getDireita());
		return qtdEsquerda + qtdDireita + 1;
	}

	public int pegaFolha() {
		return pegaFolha(mae);
	}

	private int pegaFolha(No no) {
		if (no == null) {
			return 0;
		}

		int esquerda = pegaFolha(no.getEsquerda());
		int direita = pegaFolha(no.getDireita());

		if (no.getEsquerda() == null && no.getDireita() == null) {
			return (esquerda + direita) + 1;
		} else {
			return esquerda + direita;
		}
	}

	public boolean binaria() {
		return binaria(mae);
	}

	private boolean binaria(No no) {
		if (no == null) {
			return true;
		}

		boolean esquerda = binaria(no.getEsquerda());
		boolean direita = binaria(no.getDireita());

		if ((no.getEsquerda() != null && no.getDireita() == null)
				|| (no.getEsquerda() == null && no.getDireita() != null)) {
			return false;
		}

		return esquerda && direita;

	}

	public void imprimePreOrdem() {
		imprimePreOrdem(this.mae);
	}

	private void imprimePreOrdem(No no) {

		if (no != null) {
			System.out.println(no.getValor());
			imprimePreOrdem(no.getEsquerda());
			imprimePreOrdem(no.getDireita());
		}
	}

	public void imprimeEmOrdem() {
		imprimeEmOrdem(this.mae);
	}

	private void imprimeEmOrdem(No no) {

		if (no != null) {
			imprimeEmOrdem(no.getEsquerda());
			System.out.println(no.getValor());
			imprimeEmOrdem(no.getDireita());
		}

	}

	public void imprimePosOrdem() {
		imprimePosOrdem(this.mae);
	}

	private void imprimePosOrdem(No no) {

		if (no != null) {
			imprimePosOrdem(no.getEsquerda());
			imprimePosOrdem(no.getDireita());
			System.out.println(no.getValor());
		}
	}

	public void imprimirArvore() {
		imprimirArvore(this.mae);
	}

	private void imprimirArvore(No no) {
		if (no.getEsquerda() != null) {
			imprimirArvore(no.getEsquerda());
		}

		if (no.getDireita() != null) {
			imprimirArvore(no.getDireita());
		}

		System.out.println("No: " + no.getValor());
	}

	public String preOrdemPilha() {
		return preOrdemPilha(mae);
	}

	private String preOrdemPilha(No no) {
		StringBuilder sb = new StringBuilder();
		if (no != null) {
			Stack<No> pilha = new Stack<No>();
			pilha.push(no);
			while (!pilha.isEmpty()) {
				no = pilha.pop();
				if (no != null) {
					pilha.push(no);
					sb.append(no.getValor() + ", ");
					pilha.push(no.getEsquerda());
				} else if (!pilha.isEmpty()) {
					no = pilha.pop();
					pilha.push(no.getDireita());
				}
			}

		}
		return sb.toString();

	}

	public String emOrdemPilha() {
		return emOrdemPilha(mae);
	}

	private String emOrdemPilha(No no) {
		StringBuilder sb = new StringBuilder();
		if (no != null) {
			Stack<No> pilha = new Stack<No>();
			pilha.push(no);
			while (!pilha.isEmpty()) {
				no = pilha.pop();
				if (no != null) {
					pilha.push(no);
					pilha.push(no.getEsquerda());
				} else if (!pilha.isEmpty()) {
					no = pilha.pop();
					pilha.push(no.getDireita());
					sb.append(no.getValor() + ", ");
				}
			}

		}
		return sb.toString();

	}

}
