import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ArvoreTest {
	
	private Arvore arvore;

	@BeforeEach
	void setUp() throws Exception {
		arvore = new Arvore();
	}

	@Test
	void addRaizTest() {
				arvore.add(10);
	}
	
	@Test
	void pegaQtdFolhaTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(14);
		
		assertEquals(4, arvore.pegaFolha());
				
	}
	
	@Test
	void pegaAlturaTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(14);
		
		assertEquals(4, arvore.getAltura());
	}
	
	@Test
	void arvoreBinariaTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(12);
		arvore.add(14);
		
		assertTrue(arvore.binaria());
		
	}
	
	@Test
	void arvoreNaoBinariaTest() {
		arvore.add(10);
		arvore.add(8);
		arvore.add(12);
		arvore.add(9);
		arvore.add(7);
		arvore.add(11);
		arvore.add(13);
		arvore.add(14);
		
		assertFalse(arvore.binaria());
		
	}

}
